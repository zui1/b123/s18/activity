let trainer = {
	name: "Ash Ketchum",
	age: 10,
	address: {
		city: "Pallet Town", 
		country: "Kanto Region",
	},
	friends: ["Misty","Brock","Cilan","Clemont",],
	pokemon: ["Pikachu","Caterpie","Pidgeot","Bulbasaur","Charmander"],
	catch: function(pmon){

		if(this.pokemon.length < 6){
			this.pokemon.push(pmon);
			console.log(`Gotcha ${pmon}`)
		} else {
			console.log("A trainer should only have 6 pokemons to carry.")
		}
		

	},
	release: function(){

		if(this.pokemon.length === 0){
			console.log("You have no more pokemons! Catch on first.")
		} else {
			this.pokemon.pop();
		}

	},
}

console.log(trainer.catch("Ac"))
console.log(trainer.catch("Acs"))


function Pokemon(name,type,level){
	this.name = name;
	this.type = type;
	this.level = level;
	this.hp = 3 * level;
	this.atk = 2.5 * level;
	this.def = 2 * level;
	this.isFainted = false;
	this.tackle = function(nameOfArgumentPokemon){

		console.log(`${this.name} tackled ${nameOfArgumentPokemon.name}`);

	
	}
	this.faint = function(nameOfArgumentPokemon){
		alert(`${nameOfArgumentPokemon.name} has fainted.`)
		nameOfArgumentPokemon.isFainted = true;
	}
}

pokemon1 = new Pokemon("Charizard","Flying",35);
pokemon2 = new Pokemon("Charmander","Fire",30);

console.log(pokemon1);
console.log(pokemon2);